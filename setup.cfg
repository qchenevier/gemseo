# setuptools settings
# See https://python-packaging-user-guide.readthedocs.io
# and https://setuptools.readthedocs.io for more details.
[metadata]
name = gemseo
author = GEMSEO developers
author_email = contact@gemseo.org
url = https://gitlab.com/gemseo
project_urls =
    Documentation = https://gemseo.readthedocs.io
    Source = https://gitlab.com/gemseo/dev/gemseo
    Tracker = https://gitlab.com/gemseo/dev/gemseo/-/issues
description = Generic Engine for Multi-disciplinary Scenarios, Exploration and Optimization
long_description = file: README.rst
long_description_content_type = text/x-rst
license = GNU Lesser General Public License v3
license_files =
    LICENSE.txt
    CREDITS.rst
classifiers =
    License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)
    Intended Audience :: Science/Research
    Topic :: Scientific/Engineering
    Operating System :: POSIX :: Linux
    Operating System :: MacOS
    Operating System :: Microsoft :: Windows
    Programming Language :: Python :: 2
    Programming Language :: Python :: 2.7
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3.6
    Programming Language :: Python :: 3.7
    Programming Language :: Python :: 3.8

[options]
package_dir =
    =src
packages = find:
include_package_data = True
python_requires = >=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*, <3.9
install_requires =
    custom_inherit ==2.3.1
    # the python 2.7 version is in gemseo.third_party
    fastjsonschema <=2.14.5 ; python_version>='3'
    future
    # h5py 3+ does not work with python 2.7 and has API changes
    h5py >=2.3,<=2.10.0
    matplotlib >=2,<=3.3.3
    # Temporary fix for numpy and Win10 2004
    # https://developercommunity.visualstudio.com/content/problem/1207405/fmod-after-an-update-to-windows-2004-is-causing-a.html
    numpy >=1.10,<=1.19.3 ; platform_system=='Windows'
    numpy >=1.10,<=1.19.4 ; platform_system!='Windows'
    # pathlib is in the standard library of python 3
    pathlib2 ; python_version<'3'
    pyxdsm <2.1.3 ; python_version<'3'
    pyxdsm <=2.1.3 ; python_version>='3'
    requests
    scipy >=1.1,<=1.4.1
    six
    tqdm >=4,<=4.54.0
    typing ; python_version < '3'
    xdsmjs ==1.0.0

[options.packages.find]
where = src

[options.package_data]
gemseo =
    algos/*/options/*.json
    core/*.json
    post/*.json
    problems/*/*.json
    uncertainty/*/*.json
    problems/*/*_design_space.txt
    */*/LICENSE.txt
    third_party/pyxdsm/diagram_styles.tex
    problems/dataset/iris.data
    wrappers/icons/*.png

[options.extras_require]
# dependencies for the additional features
all =
    openturns >=1.13,<=1.15
    pandas >=0.16,<=1.1.4
    pdfo ==1.0.0 ; platform_system!='Windows'
    pydoe2 >=0.3.8,<=1.3.0 ; python_version>='3'
    pydoe >=0.3.8,<=1.3.0 ; python_version<'3'
    scikit-learn >=0.18,<=0.23.2
    sympy >=0.7,<=1.7
    # for pandas excel reader
    openpyxl <=3.0.5 ; python_version>='3'
    xlwings <=0.21.4 ; platform_system=='Windows'
    #====================================================================================
    # NOTE: below packages shall be synced with requirements/gemseo-conda-python{2,3}.txt
    #====================================================================================
    # graphviz requires the dot executable, it can be installed with conda
    graphviz <=2.42.3
    # nlopt for python 2.7 or windows package is broken on pypi, install it with conda
    nlopt >=2.4.2,<=2.6.2 ; (platform_system=='Windows' and python_version>='3.7') or python_version>='3'
    # there is no pyside2 for windows with python 2.7 in pypi, install it with conda
    pyside2 <=5.15.2 ; platform_system!='Windows' and python_version>='3'
# dependencies for testing
test =
    pytest
    pytest-cov

[options.entry_points]
console_scripts =
    gemseo-study = gemseo.utils.study_analysis_cli:main
    gemseo-template-grammar-editor = gemseo.wrappers.template_grammar_editor:main [all]

[bdist_wheel]
universal = 1

# tools settings

[versioneer]
VCS = git
style = pep440
versionfile_source = src/gemseo/_version.py
versionfile_build = gemseo/_version.py
tag_prefix =

[tool:pytest]
testpaths = tests
addopts =
    --disable-pytest-warnings
     # show extra info on xfailed, xpassed, and skipped tests
    -rxs
; filterwarnings =
;     ignore::pytest.PytestExperimentalApiWarning

[coverage:run]
branch = True
source = gemseo
omit =
    src/gemseo/_version.py
    src/gemseo/third_party/*
    src/gemseo/algos/opt/lib_snopt.py
    src/gemseo/post/core/colormaps.py
    src/gemseo/wrappers/template_grammar_editor.py
    src/gemseo/wrappers/xls_discipline.py

[coverage:report]
exclude_lines =
    pragma: no cover
    raise NotImplementedError
    if __name__ == .__main__.:
    plt.show()
    pylab.plt.show()

[flake8]
# See http://www.pydocstyle.org/en/latest/error_codes.html for more details.
# https://github.com/PyCQA/flake8-bugbear#how-to-enable-opinionated-warnings
ignore =
    E501
    # no docstring for standard and private methods
    D105
select = B,C,D,E,F,G,N,T,W,B950
# settings for compatibility with black, see
# https://github.com/psf/black/blob/master/docs/compatible_configs.md#flake8
# https://black.readthedocs.io/en/stable/the_black_code_style.html?highlight=bugbear#line-length
max-line-length = 80
extend-ignore = E203, W503
per-file-ignores =
    # ignore docstring that have not yet be converted to the google style
    src/gemseo/algos/*:D
    src/gemseo/caches/*:D
    src/gemseo/core/*:D
    src/gemseo/formulations/*:D
    src/gemseo/mda/*:D
    src/gemseo/mlearning/*:D
    src/gemseo/post/*:D
    src/gemseo/problems/*:D
    src/gemseo/utils/*:D
    src/gemseo/wrappers/*:D
    src/gemseo/api.py:D
    tests/*.py:D
    # also ignore print statements violations in the examples and tutorials
    doc_src/*.py:T,D
docstring-convention = google

# [mypy]
# files = src/pytest_executable, tests
# python_version = 3.7
# warn_unused_configs = True
# # disallow_subclassing_any = True
# disallow_any_generics = True
# disallow_untyped_calls = True
# # disallow_untyped_defs = True
# disallow_incomplete_defs = True
# # check_untyped_defs = True
# disallow_untyped_decorators = True
# no_implicit_optional = True
# warn_redundant_casts = True
# warn_unused_ignores = True
# # warn_return_any = True
# # no_implicit_reexport = True
#
# [mypy-numpy,pytest,_pytest.*,py,setuptools,jsonschema,delta]
# ignore_missing_imports = True
#
# [mypy-pytest_executable._version]
# ignore_errors = True

[isort]
# settings for compatibility with black, see
# https://github.com/psf/black/blob/master/docs/compatible_configs.md#isort
multi_line_output = 3
include_trailing_comma = True
force_grid_wrap = 0
use_parentheses = True
ensure_newline_before_comments = True
line_length = 88
